package com.example.demo.greeting;

import java.util.concurrent.atomic.AtomicLong;

public class Greeting {
    private static final AtomicLong counter = new AtomicLong();
    private static final String template = "Hello, %s!";
    private final long id = counter.incrementAndGet();
    private final String name;

    public Greeting(String name) {
        this.name = String.format(template, name);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
