package com.example.demo.greeting;

import org.springframework.web.bind.annotation.*;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/greeting")
public class GreetingController {
    private final Map<Long, Greeting> greetings = new HashMap();

    @GetMapping
    public Greeting greet(@RequestParam(value = "name", defaultValue = "World") String name){
        Greeting g = new Greeting(name);
        greetings.put(g.getId(), g);
        return g;
    }

    @GetMapping("/{id}")
    public Greeting getGreeting(@PathVariable Long id) {
        return greetings.get(id);
    }

    
    @PostMapping
    public PostResult doSth(@RequestBody Person person) {
        String result = person.getFirstName() + " " + person.getLastName();
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        return new PostResult(result, hour);
    }

    private class PostResult {
        private final String result;
        private final Integer hour;

        public PostResult(String result, Integer hour) {
            this.result = result;
            this.hour = hour;
        }

        public String getResult() {
            return result;
        }

        public Integer getHour() {
            return hour;
        }
    }
}
